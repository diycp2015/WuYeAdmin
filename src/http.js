import axios from 'axios'
import Vue from 'vue';
import router from './router'

const http =  axios.create({

    // 根据服务器的地址来进行改变，不能写死
    // baseURL: process.env.VUE_APP_API_URL || '/admin/api',
    baseURL:'http://localhost:3000/admin/api'
})

// 给http的请求加错误拦截器
http.interceptors.request.use(function (config) {

    if(sessionStorage.token){
        // Bearer后面一定要加空格
        config.headers.Authorization = 'Bearer ' + (sessionStorage.token || '')
    }
    return config; 
},function(error){
    return Promise.reject(error);
})

// 给http的请求加错误拦截器
http.interceptors.response.use( response => {
    return response
}, err => {
    //错误请求>200的会进入err,使用element-ui的弹窗提示
    if(err.response.data.message)
    {
        Vue.prototype.$message({
            type: 'error',
            message: err.response.data.message
        }) 

        if(err.response.status === 401){
            router.push('/login')
        }   
    }
    return Promise.reject(err)
})

export default http;